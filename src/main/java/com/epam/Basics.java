package com.epam;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;

/**
 *
 * @version 1.0
 * @author Соломія
 */
public final class Basics {
    /**
    * static int countOdd.
    */
    private static int countOdd = 0;
    /**
     * static int countEven.
     */
    private static int countEven = 0;
    /**
     * @param array interval of numbers
     * @param size size of interval
     * @return odds
     */
    static int findOdd(final int[] array, final int size) {
        /* find odd numbers */
        int sum = 0;
        System.out.println("Odd numbers: ");
        for (int i = 0; i < size; i++) {
            if (array[i] % 2 == 0) {
                countOdd++;
                System.out.print(array[i] + " ");
            }
        }
        System.out.println();
        return sum;
    }

    /**
     * @param array interval of numbers
     * @param size sixe of interval
     * @return even numbers
     */
    static int findEven(final int[] array, final int size) {
        //find even numbers
        int sum = 0;
        System.out.println("Even numbers: ");
        for (int i = 0; i < size; i++) {
            if (array[i] % 2 != 0) {
                System.out.print(array[i] + " ");
                countEven++;
                sum += array[i];
            }
        }
        System.out.println();
        return sum;
    }

    /**
     * @param index Last number of Fibonachi
     * @return Value of Fibbonach
     */
   static int fib(final int index) {
       //solve number of Fibbonachi
        if (index == 0) {
            return 0;
        } else if (index == 1) {
            return 1;
        } else if (index == 2) {
            return 1;
        } else {
            return fib(index - 1) + fib(index - 2);
        }
    }

    /**
     * @param args final
     * @throws IOException BufferedReader
     */
    public static void main(final String[] args) throws IOException {
        BufferedReader br = new BufferedReader(
                new InputStreamReader(System.in, StandardCharsets.UTF_8));
        int intervalFrom = Integer.parseInt(br.readLine());
        int intervalTo = Integer.parseInt(br.readLine());
        int sumOdd;
        int sumEven;
        final int size = (intervalTo - intervalFrom) + 1;
        final int[] array = new int[size];
        for (int i = 0, from = intervalFrom; i < size; i++, from++) {
            array[i] = from;
        }
        sumOdd = findOdd(array, size);
        System.out.println("Summary of odd numbers = " + sumOdd);
        sumEven = findEven(array, size);
        System.out.println("Summary of even numbers = " + sumEven);
        int n = Integer.parseInt(br.readLine());
        int[] fibbonachi = new int[n];
        for (int i = 0; i < n; i++) {
            fibbonachi[i] = fib(i);
            System.out.println(fibbonachi[i]);
        }
        sumOdd = findOdd(fibbonachi, n);
        System.out.println("Summary of odd numbers = " + sumOdd);
        sumEven = findEven(fibbonachi, n);
        System.out.println("Summary of even numbers = " + sumEven);
        int f1;
        int f2;
        if (n % 2 == 0) {
            f1 = fibbonachi[n  - 1];
            f2 = fibbonachi[n - 2];
        } else {
            f1 = fibbonachi[n - 2];
            f2 = fibbonachi[n - 1];
        }
        System.out.println("F1 = " + f1);
        System.out.println("F2 = " + f2);
        double iodd = (double) countOdd / n;
        double ieven = (double) countEven / n;
        System.out.println("Imov odd = " + iodd);
        System.out.println("Imov even = " + ieven);


    }
}
